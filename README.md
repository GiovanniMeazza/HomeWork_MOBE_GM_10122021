Tramite UI è possibile:

1- attivare o disabilitare gli sputapalline
2- modificare l'intervallo di tempo tra una routine di spari e la successiva [0,75s : 3s]
3- decidere quante palline verranno generate ad ogni routine di spari [1 : 3]
4- decidere il tipo di pallina da sparare [Random , S , M , L]

ad esempio è possibile sparare per ogni sputapalline:
    2 palline medie ogni 1,5s
    1 pallina grande ogni 3s
    3 palline di tipo random ogni 0,75s
    etc.
