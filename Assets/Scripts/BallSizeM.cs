using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSizeM : MonoBehaviour
{
    private bool safe = true;
    public GameObject ballS;
    private float Speed = 150f;
    WaitForSeconds delayBallSpawn = new WaitForSeconds(0.2f);
    float timeOnFloor;


    void Start()
    {
        Invoke("SafeOff", 0.2f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Floor")
        {
            if (safe)
            {
                if (collision.collider.tag != "Gun" && collision.collider.tag != "BallSizeM" && collision.collider.tag != "Walls")
                {
                    Debug.Log("Destroyed safe by : " + collision.collider.name);
                    DestroyMBall();
                }
            }
            else
            {
                Debug.Log("Destroyed after safe by : " + collision.collider.name);
                DestroyMBall();
            }

        }

    }

    void SafeOff() { safe = false;}

    void DestroyMBall()
    {
        Destroy(gameObject.GetComponent<Collider>());
        CreateChildren(5, ballS);
        Destroy(gameObject);
    }

    void CreateChildren(int nChildren, GameObject child)
    {
        for (int i = 0; i < nChildren; i++)
        {
            GameObject newBall = Instantiate(child, transform.position + transform.forward * 1.5f, child.transform.rotation);
            Rigidbody rig = newBall.GetComponent<Rigidbody>();
            // rig.AddForce(transform.forward * Speed, ForceMode.Impulse);
            rig.AddForce((new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100))) * Speed);
        }

    }

    void OnTriggerExit(Collider cage)
    {
        if (cage.name == "Cage")
        {
            Debug.Log("Oggetto uscito di scena");
            Destroy(gameObject);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            timeOnFloor += Time.deltaTime;
            float seconds = timeOnFloor % 60;
            if (seconds >= 3)
            {
                Destroy(gameObject);
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Floor")
            timeOnFloor = 0;
    }
}
