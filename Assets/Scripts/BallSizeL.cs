using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controllo del comportamento delle Ball di taglia L
public class BallSizeL : MonoBehaviour
{
    private bool safe = true;
    public GameObject ballM;
    private float Speed = 750f;
    WaitForSeconds delayBallSpawn = new WaitForSeconds(0.03f);
    float timeOnFloor;
    
    
    void Start()
    {
       Invoke("SafeOff", 0.2f);  
    }

    
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Floor")
        {
            if (safe)
            {
                if (collision.collider.tag != "Gun" && collision.collider.tag != "BallSizeL" && collision.collider.tag != "Walls")
                {
                     Debug.Log("Destroyed safe by : " + collision.collider.name);
                    DestroyLBall();
                }
            }
            else
            {
                 Debug.Log("Destroyed after safe by : " + collision.collider.name);
                DestroyLBall();
            }

        }

    }

    void SafeOff() { safe = false; }

    void DestroyLBall()
    {
        Destroy(gameObject.GetComponent<Collider>());
        
        CreateChildren(2, ballM);
        Destroy(gameObject);
    }


    void CreateChildren(int nChildren, GameObject child)
    {
        for(int i = 0; i < nChildren; i++)
        {
            GameObject newBall = Instantiate(child, transform.position + transform.forward * 1.5f, child.transform.rotation);
            Rigidbody rig = newBall.GetComponent<Rigidbody>();
            //rig.AddForce(transform.forward * Speed, ForceMode.Impulse);
            rig.AddForce((new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100))) * Speed );
        }

    }

    void OnTriggerExit(Collider cage)
    {
        if (cage.name == "Cage")
        {
            Debug.Log("Oggetto uscito di scena");
            Destroy(gameObject);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            timeOnFloor += Time.deltaTime;
            float seconds = timeOnFloor % 60;
            if (seconds >= 3)
            {
                Destroy(gameObject);
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Floor")
            timeOnFloor = 0;
    }


}
