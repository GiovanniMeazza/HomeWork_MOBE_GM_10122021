using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float rotationSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ChangeAngle());
    }

    // Update is called once per frame
    void Update()
    {
        // transform.Rotate(0,  rotationSpeed * Mathf.Sin(rotationAngle), 0, Space.Self);
        transform.Rotate(0, rotationSpeed , 0, Space.Self);
    }

    IEnumerator ChangeAngle()
    {
        while (true)
        {
            if (rotationSpeed < 3f)
                rotationSpeed += 0.2f;
            else
                rotationSpeed = 1f;
            yield return new WaitForSeconds(0.7f);
        }
    }

}
