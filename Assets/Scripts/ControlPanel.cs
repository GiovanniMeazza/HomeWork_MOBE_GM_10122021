using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPanel : MonoBehaviour
{
    public GameObject _gun;
    private Transform _activeButton;
    private Transform _rateText;
    private Transform _nBallsText;
    private Transform _typeText;
    private int cachedType, cachedNumBalls;
    private float cachedRate;
    private bool cachedIsShooting;

    void Awake()
    {
        _activeButton = this.gameObject.transform.GetChild(1);
        _rateText = this.gameObject.transform.GetChild(2);
        _nBallsText = this.gameObject.transform.GetChild(3);
        _typeText = this.gameObject.transform.GetChild(4);
    }


    void Start()
    {
        CheckIsShooting();  
        CheckRate();
        CheckNBalls();
        _typeText.GetComponent<Text>().text = "Rand";
    }


    void Update()
    {
        CheckIsShooting();  
        CheckRate();
        CheckNBalls();
        CheckBallType();   
    }

    public void SwitchShooting()
    {
        _gun.GetComponent<Guns>().SwitchShooting();
    }

    void CheckIsShooting()
    {
        if(_gun.GetComponent<Guns>().isShooting != cachedIsShooting)
        {
            cachedIsShooting = _gun.GetComponent<Guns>().isShooting;
            if (cachedIsShooting)
                _activeButton.GetComponentInChildren<Text>().text = "Turn Off";
            else
                _activeButton.GetComponentInChildren<Text>().text = "Turn On";
        }
    }

    public void ChangeRate(float rateMod)
    {
        _gun.GetComponent<Guns>().ChangeShootingSpeed(rateMod);
    }

    void CheckRate()
    {
        if (_gun.GetComponent<Guns>().delayShooting != cachedRate)
        {
            cachedRate = _gun.GetComponent<Guns>().delayShooting;
            float roundedCurrentRate = (float)System.Math.Round((double)cachedRate, 2);
            _rateText.GetComponent<Text>().text = roundedCurrentRate.ToString() + "s";
        }
    }

    public void ChangeNBAlls(float newNum)
    {
        _gun.GetComponent<Guns>().ChangeNumBalls(newNum);
    }

    void CheckNBalls()
    {
        if(cachedNumBalls != _gun.GetComponent<Guns>().nBallsXShoot)
        {
            cachedNumBalls = _gun.GetComponent<Guns>().nBallsXShoot;
            _nBallsText.GetComponent<Text>().text = _gun.GetComponent<Guns>().nBallsXShoot.ToString();
        }
    }

    public void ChangeBallType(float newType)
    {
        _gun.GetComponent<Guns>().ChangeBallType(newType);
    }

    void CheckBallType()
    {
        if(cachedType != _gun.GetComponent<Guns>().ballType)
        {
            cachedType = _gun.GetComponent<Guns>().ballType;
            switch (cachedType)
            {
                case 0:
                    _typeText.GetComponent<Text>().text = "Rand";
                    break;
                case 1:
                    _typeText.GetComponent<Text>().text = "S";
                    break;
                case 2:
                    _typeText.GetComponent<Text>().text = "M";
                    break;
                case 3:
                    _typeText.GetComponent<Text>().text = "L";
                    break;
            }
        }

    }





}
