using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFloating : MonoBehaviour
{
    private bool moveUp= true;
    private  float speed= 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SwitchMovement", 0, 0.3f);
    }

    // Update is called once per frame
    void Update()
    {   
        if (moveUp)
            transform.Translate(0.1f * speed,  0.1f * speed, 0);
        else
            transform.Translate(-0.1f * speed, -0.1f * speed, 0);
    }

    void SwitchMovement()
    {
        moveUp = !moveUp;
    }

  
}
