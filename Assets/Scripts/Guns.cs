using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Guns : MonoBehaviour
{
    public bool isShooting = true;
    public GameObject ballL, ballM, ballS;
    public Transform spawnerPos;
    public float shootingSpeed = 1.5f; 
    private float ballsSpeed = 150f;
    System.Random rand = new System.Random();
    WaitForSeconds delayBallSpawn = new WaitForSeconds(0.15f);
    public float delayShooting;
    public int ballType = 0;
    public int nBallsXShoot = 1;

    void Awake()
    {
        delayShooting = shootingSpeed;
    }


    void Start()
    {
        StartCoroutine(ShootingCoroutine());
    }

    void Update()
    {

    }

    IEnumerator ShootingCoroutine() 
    {
        while (isShooting)
        {
            Shoot();
            yield return new WaitForSeconds(delayShooting);
        }
        
    }

    void Shoot()
    {
        int selectedBallType;

        if (ballType == 0)
            selectedBallType = rand.Next(1, 4);
        else
            selectedBallType = ballType;

        switch (selectedBallType)
        {
            case 1:
                StartCoroutine(SpawnBallCoroutine(nBallsXShoot, ballS));
                break;
            case 2:
                StartCoroutine(SpawnBallCoroutine(nBallsXShoot, ballM));
                break;
            case 3:
                StartCoroutine(SpawnBallCoroutine(nBallsXShoot, ballL));
                break;
        }
    }

    public void ChangeShootingSpeed(float speedMod) 
    {
        delayShooting = shootingSpeed * speedMod;
    }

    public void ChangeNumBalls(float newNum)
    {
        nBallsXShoot = (int)newNum;
    }

    public void ChangeBallType(float newType)
    {
        ballType = (int)newType;
    }

    void CreateBall(GameObject ball)
    {
            GameObject newBall = Instantiate(ball, spawnerPos.position + transform.forward * 1.5f, ball.transform.rotation);
            Rigidbody rig = newBall.GetComponent<Rigidbody>();
            rig.AddForce(transform.forward * ballsSpeed * rig.mass, ForceMode.Impulse);
    }

    IEnumerator SpawnBallCoroutine(int nBalls, GameObject ball)
    {
        int i = 0;

        while (i < nBalls)
        {
            CreateBall(ball);
            i++;
            yield return delayBallSpawn;
        }

    }


    public void SwitchShooting()
    {
        isShooting = !isShooting;
        if(isShooting)
            StartCoroutine(ShootingCoroutine());
    }
}
