using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StuffShooter : MonoBehaviour
{
    float speed = 0.2f;
    public bool direction = true;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ChangeDirection", 0f,0.3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (direction)
            transform.Translate(0,0, 1 * speed , Space.Self);
        else
            transform.Translate(0, 0, -1 * speed, Space.Self);
    }

    public void ChangeDirection()
    {
        direction = !direction;    
    }
}
