using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSizeS : MonoBehaviour
{
    private bool safe = true;
    float timeOnFloor;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("SafeOff", 0.2f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Floor")
        {
            if (safe)
            {
                if (collision.collider.tag != "Gun" && collision.collider.tag != "BallSizeS" && collision.collider.tag != "Walls")
                {
                    Debug.Log("Destroyed safe by : " + collision.collider.name);
                    Destroy(gameObject);
                }
            }
            else
            {
                Debug.Log("Destroyed after safe by : " + collision.collider.name);
                Destroy(gameObject);
            }
        }
    }

    void SafeOff() { safe = false; }

    void OnTriggerExit(Collider cage)
    {
        if (cage.name == "Cage")
        {
            Debug.Log("Oggetto uscito di scena");
            Destroy(gameObject);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            timeOnFloor += Time.deltaTime;
            float seconds = timeOnFloor % 60;
            if (seconds >= 3)
            {
                Destroy(gameObject);
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Floor")    
            timeOnFloor = 0;
    }
}